import kotlinx.coroutines.*

suspend fun doWork()
{
    delay(1000L)
    print("World")
}

suspend fun main()= coroutineScope{
    launch{ doWork() }
    delay(2000L)
    print("Hello,")
}